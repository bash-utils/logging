#!/bin/bash

prep_dir(){
    echo -n "mkdir -p "${1}":   "
    if ! [ -d "${1}" ]; then 
        mkdir -p "${1}" && echo "OK" || { echo "FAIL"; return 1; }
    fi
}

install(){
    # import configured paths
    source .config/FIELDS || { echo "run ./configure first"; exit 4; }
    prep_dir "${LOGGING_CONFIG_DIR}"     || return 1
    cp -vr --parent etc/* "${LOGGING_CONFIG_DIR}" || return 1
    prep_dir "${LOGGING_INCLUDE_DIR}"    || return 2
    cp -vr --parent bin/* "${LOGGING_INCLUDE_DIR}"|| return 2
    prep_dir "${LOGGING_INSTALL_DIR}"    || return 3
    ln -vs "${LOGGING_INCLUDE_DIR}/main.sh" \
           "${LOGGING_INSTALL_DIR}/logging"|| return 3
    cat <<- EOF > "${LOGGING_INCLUDE_DIR}/.PATHS"
LOGGING_SELF="${LOGGING_INSTALL_DIR}/logging"
LOGGING_CONFIG_DIR="${LOGGING_CONFIG_DIR}"
LOGGING_INCLUDE_DIR="${LOGGING_INCLUDE_DIR}"
LOGGING_INSTALL_DIR="${LOGGING_INSTALL_DIR}"
EOF
    echo '0.7.1' > "${LOGGING_INCLUDE_DIR}/.VERSION"
}

clean(){
    rm -ri "${LOGGING_CONFIG_DIR}"
    rm -ri "${LOGGING_INCLUDE_DIR}"
    rm -f  "${LOGGING_INSTALL_DIR}/logging"
    rm -ri "${LOGGING_INSTALL_DIR}"
}


### MAIN ###

# change to working directory
cd "$(dirname "$0")"

case "${1}" in
    ''|install)
        install || { status=$?; clean; exit ${status}; }
        ;;
    clean|uninstall)
        exit $(clean);
        ;;
esac
