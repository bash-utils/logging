#!/bin/bash

# DEBUG initialization
LOGGING_DEBUG_STREAM=28
if [ $DEBUG ]; then eval "exec ${LOGGING_DEBUG_STREAM}>&2" 
else eval "exec ${LOGGING_DEBUG_STREAM}>/dev/null" 
fi
LOGGING_DEBUG(){
    eval "printf '%0.s+' {2..${#FUNCNAME[@]}} >&$LOGGING_DEBUG_STREAM"
    #local args="$@"
    eval "$@ >&$LOGGING_DEBUG_STREAM"
}

logging_version(){
    LOGGING_DEBUG echo "logging_version"
    if [[ "${1}" == -h ]]; then
        echo "prints current version string and exits"
        return
    fi
    cat "${LOGGING_INCLUDE_DIR}/.VERSION"
    exit
}

logging_usage(){
    LOGGING_DEBUG echo "logging_usage"
    if [[ "${1}" == -h ]]; then
        echo "prints help message and exits"
        return
    fi
    echo "USAGE: ${0} [OPTIONS]"
    echo "OPTIONS:"
    # output verbosity
    echo "  -v|--verbose         outputs lots of useful information"
    echo "  -q|--quiet           outputs only error messages"
    echo "  -s|--silent          virtually suppresses output"
    echo "  --logging-level=LVL  specifies a colon-separated list of"
    echo "                       logging streams to enable"
    # output formatting
    echo "  --logging-*-format=STR sets stream-specific or default"
    echo "                         formatting to \$STR"
    echo "  --logging-*-output=DSC specifies that stream-specific or"
    echo "                         all output be redirected to \$DSC"
    # logging-specific options come here
    echo "  --logging-nocapture   only long --logging-* options will"
    echo "                        be interpreted from now on"
    # general options
    echo "  -h|--help           $(usage -h)"
    echo "  -V|--version        $(version -h)"
    exit
}

logging_parse_opts(){
    LOGGING_DEBUG echo "logging_parse_opts $@"
    declare -a carr
    while [ "${1}" ]; do
        LOGGING_DEBUG echo "parsing option $@"
        case "${1}" in
            # output verbosity
            -s|--silent)  (($LOGGING_NOCAPTURE)) || set_logging_level S
                          ;;
            -q|--quiet)   (($LOGGING_NOCAPTURE)) || set_logging_level Q
                          ;;
            -v|--verbose) (($LOGGING_NOCAPTURE)) || set_logging_level V
                          ;;
            --logging-level=*)
                local spec=${1##*=}
                set_logging_level C ${spec//:/ }
                ;;
            # output formatting
            # calls either logging_* or $STREAM_* depending on option
            --logging*format=*)
                local act=${1%%=*}
                local spec=${1##*=}
                TFS=$IFS
                IFS='-'
                carr=( $act )
                IFS=$TFS
                eval "${carr[-2]}_${carr[-1]} ${spec}"
                ;;
            --logging*output=*)
                local act=${1%%=*}
                local spec=${1##*=}
                TFS=$IFS
                IFS='-'
                carr=( $act )
                IFS=$TFS
                eval "${carr[-2]}_${carr[-1]} ${spec}"
                ;;
            # logging-specific options
            --) LOGGING_SOURCED=1 ;;
            --logging-nocapture) LOGGING_NOCAPTURE=1 ;;
            # general options
            -h|--help)    (($LOGGING_SOURCED)) || logging_usage ;;
            -V|--verbose) (($LOGGING_SOURCED)) || logging_version ;;
            *)
                echo "ERR_ARGVAL##unrecognised option: ${1}" >&2
                echo "            please consult ${BASH_SOURCE[0]} --help" >&2
                exit 1
                ;;
        esac
        shift
    done
}

### MAIN ###
LOGGING_DEBUG echo "MAIN:${PWD}##${BASH_SOURCE[@]}"

# resolve CDPATH bug leading to $(cd)-ed paths containing top dir twice
unset CDPATH

## MAIN.ENTER ##
# initialization section - changing working dir, setting SHOPT etc
# check if sourced, import modules
LOGGING_SOURCED=0
if [[ "${BASH_SOURCE[0]}" != "$0" ]]; then LOGGING_SOURCED=1; fi
LOGGING_DEBUG echo "running sourced: ${LOGGING_SOURCED}"
: ${LOGGING_MODULES:=core:streams:formatting}
TFS="$IFS"
IFS=':'
for mod in ${LOGGING_MODULES}; do 
    IFS="$TFS"
    LOGGING_DEBUG echo "loading module ${mod}..."
    if   [[ -e "./${mod}"    ]]; then source "./${mod}"
    elif [[ -e "./${mod}.sh" ]]; then source "./${mod}.sh"
    elif [[ -e "${mod}"      ]]; then source "${mod}"
    else echo "${BASH_SOURCE[0]} does not contain module ${mod}" >&2
    fi
    IFS=':'
done
IFS="$TFS"

# find actual working directory
SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink -e "${SOURCE}")"; done
CDIR="${PWD}"
WDIR="$(dirname "${SOURCE}")"
cd "${WDIR}"
LOGGING_DEBUG echo "source directory: ${WDIR}"

# read pathconfig
source .PATHS

## MAIN.RUN ##
: ${LOGGING_NOCAPTURE:=0}
logging_parse_opts $@
# only run if not sourcing
if ! ((LOGGING_SOURCED))
then LOGGING_DEBUG echo "executing MAIN block"
    # any actual action performed goes here
fi

## MAIN.EXIT ##
# state restoration
cd "${CDIR}"
