#!/bin/bash

shopt -s extdebug

source main.sh

test(){
    eval "echo -e \"${!LOGGING_STREAMS[@]}\" >&${LOGGING_DEFAULT_STREAMS[2]}"
    eval "echo -e \"${LOGGING_ENABLED[@]}\" >&${LOGGING_DEFAULT_STREAMS[2]}"
    err   "ERR   test"
    info  "INFO  test"
    warn  "WARN  test"
    debug "DEBUG test"
}

eval "echo -e \"\n\n## test ${LOGGING_LEVEL} ##\" >&${LOGGING_DEFAULT_STREAMS[2]}"
dry_run
eval "echo >&${LOGGING_DEFAULT_STREAMS[2]}"
test

set_logging_level S
eval "echo -e \"\n\n## test ${LOGGING_LEVEL} ##\" >&${LOGGING_DEFAULT_STREAMS[2]}"
dry_run
eval "echo >&${LOGGING_DEFAULT_STREAMS[2]}"
test

set_logging_level Q
eval "echo -e \"\n\n## test ${LOGGING_LEVEL} ##\" >&${LOGGING_DEFAULT_STREAMS[2]}"
dry_run
eval "echo >&${LOGGING_DEFAULT_STREAMS[2]}"
test

set_logging_level V
eval "echo -e \"\n\n## test ${LOGGING_LEVEL} ##\" >&${LOGGING_DEFAULT_STREAMS[2]}"
dry_run
eval "echo >&${LOGGING_DEFAULT_STREAMS[2]}"
test

set_logging_level C err warn
eval "echo -e \"\n\n## test ${LOGGING_LEVEL} ##\" >&${LOGGING_DEFAULT_STREAMS[2]}"
dry_run
eval "echo >&${LOGGING_DEFAULT_STREAMS[2]}"
test

set_logging_level C ! err info
eval "echo -e \"\n\n## test ${LOGGING_LEVEL} ##\" >&${LOGGING_DEFAULT_STREAMS[2]}"
dry_run
eval "echo >&${LOGGING_DEFAULT_STREAMS[2]}"
test

#set_logging_level V
#eval "echo -e \"\n\n## test logging_format ##\" >&${LOGGING_DEFAULT_STREAMS[2]}"
#dry_run
#eval "echo >&${LOGGING_DEFAULT_STREAMS[2]}"
#logging_format "%lvl%## %msg%" info
#logging_format "%lvl%::%src%::%msg%" err
#test
#eval "echo >&${LOGGING_DEFAULT_STREAMS[2]}"
#logging_format_selective ! "%usr%-%pcs%## %msg%" err
#test

set_logging_level V
eval "echo -e \"\n\n## test logging_output ##\" >&${LOGGING_DEFAULT_STREAMS[2]}"
logging_output "warn" warn
logging_output "info" info debug
test
eval "echo >&${LOGGING_DEFAULT_STREAMS[2]}"
logging_output_selective ! "debugerr" warn info
logging_output "warninfo" warn info
test
