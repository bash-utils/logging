#!/bin/bash

dry_run(){
    LOGGING_DEBUG echo "dry run $@"
    declare i
    for i in ${!LOGGING_DEFAULT_STREAMS[@]}; do
        eval "echo DEF_STR${i}:${LOGGING_DEFAULT_STREAMS[$i]} >&${LOGGING_DEFAULT_STREAMS[$i]}"
    done
    for i in ${!LOGGING_STREAMS[@]}; do
        eval "echo ${i}:${LOGGING_STREAMS[$i]}:${LOGGING_STRTARG[$i]} >&${LOGGING_STREAMS[$i]}"
    done
}

# print message from $LEVEL to $STDOUT
log(){
    LOGGING_DEBUG echo "log $@"
    if (($#<1 || $#>2)); then 
        eval "echo \"invalid argument count: ${#} (message[,level])\" >&${LOGGING_DEFAULT_STREAMS[2]}"
        return 1
    fi
    declare m="$1" l="${2:-LOG}" s="${FUNCNAME[$#]}" d="$#"
    #declare format="${LOGGING_FORMATS[${2,,}]:-${LOGGING_DEFAULT_FORMAT}}"
    local format="$(logging_get_format "$2")"
    LOGGING_DEBUG echo "m=$m l=$l s=$s d=$d"
    LOGGING_DEBUG echo "format: $format"

    if [ $l == 'LOG' ]; then
        eval "printf \"${format}\" \"$l\" \"$s\" \"$m\" >&{LOGGING_DEFAULT_STREAMS[1]}"
    else eval "printf \"${format}\" \"${l}\" \"${s}\" \"${m}\""
    fi
}

set_logging_level(){
    LOGGING_DEBUG echo "set_logging_level $@"
    
    case "${1^^}" in
        S|SILENT)  logging_enable_selective
                   LOGGING_LEVEL=SILENT
                   ;;
        Q|QUIET)   logging_enable_selective ERR
                   LOGGING_LEVEL=QUIET
                   ;;
        ''|DEFAULT)logging_enable_selective ERR INFO
                   LOGGING_LEVEL=DEFAULT
                   ;;
        V|VERBOSE) logging_enable_selective !
                   LOGGING_LEVEL=VERBOSE
                   ;;
        C|CUSTOM)
            shift
            logging_enable_selective $@
            LOGGING_LEVEL=CUSTOM
            ;;
        *) eval "echo \"unrecognised option: $1\" >&${LOGGING_DEFAULT_STREAMS[2]}"
           return 1
           ;;
    esac
}
