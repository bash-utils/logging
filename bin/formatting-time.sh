#!/bin/bash

LOGGING_PLACEHOLDERS[':t[:;,.|_/\-]+']='$(logging_parse_time_format $X)'
LOGGING_PLACEHOLDERS[':T[:;,.|_/\-]+']='$(logging_parse_time_format $X)'

TIME_FULL_FORMAT='%Y:%m:%d:%H:%M:%S'

logging_parse_time_format(){
    local str=${1//%/} 
    local mode=${str:1:1} 
    local form=${str:2}
    local tarr=()
    TFS=$IFS
    IFS=':'
    case $mode in
        t) tarr=( $(printf "%(${TIME_FULL_FORMAT})T" -1) ) ;;
        T) tarr=( $(printf "%(${TIME_FULL_FORMAT})T" $SECONDS) )
           local i=1; while (($i<=${#tarr[@]})); do
                tarr[$i]=$((${tarr[$i]}-${EPOCH[$i]}))
           done ;;
    esac
    IFS=$TFS

}

### INITIALIZATION ###
TFS=$IFS
IFS=':'
EPOCH=( $(printf '%(%Y:%m:%d:%H:%M:%S)T' 0) )
IFS=$TFS
