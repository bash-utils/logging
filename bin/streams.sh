#!/bin/bash

# Available redirection targets:
# STDOUT STDERR STDNIL

# Available logging streams:
#  name   stream   target
# ERR        2     STDERR
# INFO       1     STDOUT
# WARN       4     STDERR
# DEBUG      3     STDOUT

# Available logging levels:
#  name     specification 
# SILENT    no output
# QUIET     only ERR
# DEFAULT   ERR + INFO
# VERBOSE   NORMAL + WARN + DEBUG
# CUSTOM    user-specified

# store original output streams
#LOGGING_DEFAULT_STREAMS=(5  6  7)
#LOGGING_DEFAULT_STREDIR=('' '' '')
declare -A LOGGING_DEFAULT_STREAMS=([0]=5 [1]=6 [2]=7)
declare -A LOGGING_DEFAULT_STREDIR
eval "exec ${LOGGING_DEFAULT_STREAMS[0]}>/dev/null"
eval "exec ${LOGGING_DEFAULT_STREAMS[1]}>&1"
eval "exec ${LOGGING_DEFAULT_STREAMS[2]}>&2"

# each stream is identified by its descriptor in
# $LOGGING_STREAMS and target in $LOGGING_STRTARG
declare -A LOGGING_STREAMS
declare -A LOGGING_STRTARG
declare -A LOGGING_STREDIR
declare -A LOGGING_ENABLED

# enables/disables specified streams
logging_enable(){
    LOGGING_DEBUG echo "logging_enable $@"
    declare name desc dest defs targ=''
    if [[ "$1" =~ ^[0\!1]$ ]]; then
        ((${1//[^0-9]/0})) || targ=">&${LOGGING_DEFAULT_STREAMS[0]}"
        shift
    fi
    for name in ${@,,}; do
        [ -n "$targ" ] && LOGGING_ENABLED["$name"]=0 \
                       || LOGGING_ENABLED["$name"]=1
        defs="${LOGGING_STRTARG[$name]}"
        desc="${LOGGING_STREAMS[$name]}"
        # lookup hierarchy:
        #  1. $targ will succeed if $name is to be disabled
        #  2. $sred will succeed if redirection for $name was set
        #  3. $dred will succeed if default stream redirection was set
        #  4. $defs will succeed otherwise
        dest="${targ:-${LOGGING_STREDIR[$name]:-${LOGGING_DEFAULT_STREDIR[$defs]:->&${LOGGING_DEFAULT_STREAMS[$defs]}}}}"
        LOGGING_DEBUG echo "$name: '${desc}${dest}'"
        if ! [[ "$name" && "$desc" && "$dest" ]]; then
            eval "echo \"invalid argument count: name/desc/dest = ${name}/${desc}/${dest}\" >&${LOGGING_DEFAULT_STREAMS[2]}"
            return 3
        fi
        eval "exec ${desc}${dest}"
    done
}

# selective enabling/disabling first disables/enables
# all streams and then enables/disables as specified
logging_enable_selective(){
    LOGGING_DEBUG echo "logging_enable_selective $@"

    declare flag=1
    if [[ "$1" =~ ^[0\!1]$ ]]; then
        flag=$((${1//[^0-9]/0}))
        shift
    fi
    logging_enable $((! $flag)) ${!LOGGING_STREAMS[@]} || return $?
    logging_enable $flag $@ || return $?
}

# register a stream of $NAME and $DESC redirecting to $TARG
logging_register(){
    LOGGING_DEBUG echo "logging_register $@"

    declare name="${1,,}" desc=$2 targ=${3:-1}
    if ((0>$targ || $targ>=${#LOGGING_DEFAULT_STREAMS[@]})); then
        targ=1
    fi

    LOGGING_DEBUG echo "'${name}: ${desc} -> ${targ}'"
    if ! [[ "$name" && "$desc" && "$targ" ]]; then
        eval "echo \"missing arg: name/descr/target = ${name}/${desc}/${targ}\" >&${LOGGING_DEFAULT_STREAMS[2]}"
        return 1
    fi
    
    LOGGING_STREAMS["$name"]=$desc
    LOGGING_STRTARG["$name"]=$targ
    eval "${name// /_}(){ log \"\${1}\" \"${name}\" >&${desc} 2>&1; }"
    eval "${name// /_}_enable(){ logging_enable \${1:-\$((! \$?))} \"${name}\"; }"
    eval "${name// /_}_format(){ logging_format \"\${1}\" \"${name}\"; }"
    eval "${name// /_}_output(){ logging_output \"\${1}\" \"${name}\"; }"
    logging_enable ! "$name" || return $?
}


### DEFAULT STREAMS ###
LOGGING_DEBUG echo 'initiating default streams'
logging_register 'err'   2 2 && LOGGING_DEBUG echo 'ERR   ok' || LOGGING_DEBUG echo 'ERR   failed'
logging_register 'info'  1 1 && LOGGING_DEBUG echo 'INFO  ok' || LOGGING_DEBUG echo 'INFO  failed'
logging_register 'warn'  4 2 && LOGGING_DEBUG echo 'WARN  ok' || LOGGING_DEBUG echo 'WARN  failed'
logging_register 'debug' 3 1 && LOGGING_DEBUG echo 'DEBUG ok' || LOGGING_DEBUG echo 'DEBUG failed'

set_logging_level DEFAULT

