#!/bin/bash

declare -A LOGGING_PLACEHOLDERS
LOGGING_PLACEHOLDERS['msg']='\${m}'
LOGGING_PLACEHOLDERS['lvl']='\${l}'
LOGGING_PLACEHOLDERS['cmd']='\${0}'
LOGGING_PLACEHOLDERS['src']='\${FUNCNAME[\${d}]}'
LOGGING_PLACEHOLDERS['pcs']='\${FUNCNAME[*]}'
LOGGING_PLACEHOLDERS['pwd']='\${PWD}'
LOGGING_PLACEHOLDERS['usr']='\${USER}'
LOGGING_PLACEHOLDERS['sec']='\${SECONDS}'

logging_parse_format(){
    LOGGING_DEBUG echo "logging_parse_format $@"
    if ! [ "$1" ]; then return; fi
    declare format="$1" X Y
    for P in ${!LOGGING_PLACEHOLDERS[@]}; do
        if [[ $1 =~ %${P}% ]]; then
            X=${BASH_REMATCH[0]}
            eval Y="${LOGGING_PLACEHOLDERS[$P]}"
            format="${format//$X/$Y}"
        fi
    done
    echo "${format}\\n"
}

: ${LOGGING_DEFAULT_FORMAT:="%lvl%[%sec%]%cmd%::%src%## %msg%"}
LOGGING_DEFAULT_FORMAT="$(logging_parse_format "$LOGGING_DEFAULT_FORMAT")"
declare -A LOGGING_FORMATS

logging_get_format(){ echo "${LOGGING_FORMATS[${1,,}]:-${LOGGING_DEFAULT_FORMAT}}"; }

# overrides formatting for selected streams
logging_format(){
    LOGGING_DEBUG echo "logging_format $@"
    declare format="$(logging_parse_format "${1}")"
    shift
    if ! [ "$1" ]; then 
        LOGGING_DEBUG echo "formatting: $format as DEFAULT_FORMAT"
        LOGGING_DEFAULT_FORMAT="$format"; 
    fi
    for name in ${@,,}; do
        LOGGING_DEBUG echo "formatting: $format for $name"
        [ "$format" ] && LOGGING_FORMATS[$name]="$format" \
                      || unset LOGGING_FORMATS[$name]
    done
}

# selective formatting first sets all formats to default
# and then overrides them as specified
logging_format_selective(){
    LOGGING_DEBUG echo "logging_format_selective $@"
    declare all='' sel=''
    if [[ "$1" =~ ^[0\!1]$ ]]; then 
        ((${1//[^0-9]/0})) && sel="$2" || all="$2"
        shift
    else sel="$1"
    fi
    shift
    logging_format "$all" ${!LOGGING_STREAMS[@]}
    logging_format "$sel" $@
}

logging_output(){
    LOGGING_DEBUG echo "logging_output $@"
    [[ "$1" =~ ^'>'+ ]] && mode="${BASH_REMATCH[0]}"
    : ${mode:='>>'}
    file="${1##*>}"
    shift
    targ="${@:-1}"
    for name in ${targ,,}; do
        if [[ "$name" =~ [012] ]]; then
            LOGGING_DEBUG echo "'setting output for DEF_STR${name} to ${mode}${file:-DEFAULT}'"
            [ "$file" ] && LOGGING_DEFAULT_STREDIR[$name]="${mode}${file}" \
                        || unset LOGGING_DEFAULT_STREDIR[$name]
        else 
            LOGGING_DEBUG echo "'setting output for ${name} to ${mode}${file:-DEFAULT}'"
            [ "$file" ] && LOGGING_STREDIR["$name"]="${mode}${file}" \
                        || unset LOGGING_STREDIR["$name"]
            logging_enable ${LOGGING_ENABLED["$name"]} "$name"
        fi
    done
}

logging_output_selective(){
    LOGGING_DEBUG echo "logging_output_selective $@"
    declare all='' sel=''
    if [[ "$1" =~ ^[0\!1]$ ]]; then
        ((${1//[^0-9]/0})) && sel="$2" || all="$2"
        shift
    else sel="$1"
    fi
    shift
    logging_output "$all" ${!LOGGING_STREAMS[@]}
    logging_output "$sel" $@
}
