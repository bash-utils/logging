# Logging (c) Aachne Dirkschneider <n45t31@protonmail.com>
## version 0.7.1
### About

**Logging** is a library providing standard logging streams to call from *bash* scripts.

### Table of Contents
- [usage](#usage)
  - [synopsis](#synopsis)
  - [options](#options)
  - [exit codes](#exit-status)
  - [examples](#examples)
- [support](#support)
  - [requirements](#prerequisites)
  - [installation](#installation)
- [description](#description)
  - [logging levels](#logging-levels)
  - [output handling](#output-handling)
    - [formatting](#format-strings)

### Usage
#### Synopsis
**logging** -h|--version <br/>
**logging** -- \[--logging-nocapture\] \[-qvs|--logging-level=*streams*\] \[--logging-\*-format=*format*\] \[--logging-\*-output=*file*\]

#### Options
###### logging levels
<table border="0">
  <tr>
    <td align="right">-v|--verbose</td>
    <td>outputs lots of useful information</td>
    <td></td>
  </tr>
  <tr>
    <td align="right">-q|--quiet</td>
    <td>default, outputs some information</td>
    <td></td>
  </tr>
  <tr>
    <td align="right">-s|--silent</td>
    <td>completely suppresses console output</td>
    <td></td>
  </tr>
  <tr>
    <td align="right">--logging-level</td>
    <td align="center">streams</td>
    <td>specifies custom logging level from colon separated list of streams</td>
  </tr>
</table>

###### output formatting
<table border="0">
  <tr>
    <td align="right">--logging-format|--logging-*-format</td>
    <td align="center">format</td>
    <td>specifies default/stream-specific C-style logging format</td>
  </tr>
  <tr>
    <td align="right">--logging-output|--logging-*-output</td>
    <td align="center">file</td>
    <td>specifies default/stream-specific output target</td>
  </tr>
</table>

###### other options
<table border="0">
  <tr>
    <td align="right">-h|--help</td>
    <td>displays help message and exits</td>
  </tr>
  <tr>
    <td align="right">-V|--version</td>
    <td>outputs version string and exits</td>
  </tr>
  <tr>
    <td align="right">--logging-nocapture</td>
    <td>only long --logging-* options will be interpreted
        from this option's first occurence onward</td>
  </tr>
</table>

#### Exit status
<table border="0">
  <thead>
    <tr>
      <th align="center">NAME</th>
      <th align="center">VALUE</th>
      <th align="center">DESCRIPTION</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td align="right">EXIT_SUCCESS</td>
      <td align="center">0</td>
      <td>shrepper succeeded</td>
    </tr>
    <tr>
      <td align="right">ERR_ARGVAL</td>
      <td align="center">1</td>
      <td>invalid command-line argument</td>
    </tr>
    <tr>
      <td align="right">ERR_OPFAIL</td>
      <td align="center">*</td>
      <td>amiguous internal error</td>
    </tr>
  </tbody>
</table>

#### Examples
- `logging --version` <br/>displays version string 
- `logging -h` <br/>displays usage help 
- `source logging -- ${@}` <br/>imports logging lib with user config 
- `source logging -- --logging-level=ERR:WARN --logging-err-format='%lvl%: %src%: %msg%'` <br/>
  imports logging lib with custom logging level and custom ERR logging format

### Support
#### Prerequisites
- bash >= 3.0.1

#### Installation
`git clone https://gitlab.com/bash-utils/logging.git`<br/>
`cd logging`<br/>
`./configure [--install-dir=DIRNAME]`<br/>
`./install.sh`<br/>
then just write `source logging -- OPTIONS` and you're good to go<br/>

### Description
**Logging** was conceived one 31st October when I finally got tired of 
rewriting the same stream redirection schema for some 2137th time and decided 
to standardise information passing facilities among all of my extant and future
bashscripts. As such I decided on a _reasonable_ degree of configurability on
which I might or might not capitalise later on (e.g. defining additional logging
streams for more fine-grained customizability using config files seems nice).

#### Logging levels
As of now, **Logging** provides 4 standard logging streams: *INFO*, *ERR*, 
*WARN* and *DEBUG*. These are roughly equivallent to logging levels as follows:

<table border="0">
  <tr><td>  </td><td align="center">ERR</td><td align="center">INFO</td><td align="center">WARN</td><td align="center">DEBUG</td></tr>
  <tr><td>-s</td><td align="center"> 0 </td><td align="center"> 0  </td><td align="center"> 0  </td><td align="center">  0  </td></tr>
  <tr><td>-q</td><td align="center"> 1 </td><td align="center"> 0  </td><td align="center"> 0  </td><td align="center">  0  </td></tr>
  <tr><td>  </td><td align="center"> 1 </td><td align="center"> 1  </td><td align="center"> 0  </td><td align="center">  0  </td></tr>
  <tr><td>-v</td><td align="center"> 1 </td><td align="center"> 1  </td><td align="center"> 1  </td><td align="center">  1  </td></tr>
</table>

Each stream is accessed via a consistent set of methods whose names begin
accordingly and follow the pattern:

- ***STREAM*(*message*)**: logs *message* to *STREAM*
- ***STREAM*_enable(*flag*)**: switches *STREAM* on/off, accepts a C-style boolean <br/>
  __NOTE__: if no argument is passed, state is deduced from *$?*, so these are both valid:
  - *STREAM*\_enable $(true)
  - *STREAM*\_enable $(false)
- ***STREAM*_format(*format*)**: sets *STREAM* formatting string
- ***STREAM*_output(*file*)**: redirects *STREAM* output to *file*

A parallel set of global methods exists as well:

- **log(*message*)**: logs default-formatted *message* to **STDOUT**
- **log(*level*,*message*)**: logs *level*-formatted *message* to **STDOUT** <br/>
  __NOTE__: this is actually implicitly called by each *STREAM*(...) and
  **SHOULD NOT BE CALLED DIRECTLY** as it assumes a one-level-deeper call stack!
- **logging-enable([*flag*,]*streams*)**: selectively enables/disables *streams* specified
  as a colon-separated list <br/>
  __NOTE__: *flag* can be a C-style boolean or ! (negative, synonym for 0)
- **logging_format(*format*)**: sets default formatting string
- **logging_output(*file*)**: sets default output to *file*

#### Output handling
By default, **Logging** uses *printf* bash built-in for handling information 
output. This allows for custom formatting strings to be applied to each logging 
stream separately, each potentially containing various *%placeholders%* computed
once on calling \*\_format(...).

Vanilla fallback formatting is: `%lvl%[%t:::%]%cmd%::%src%## %msg%`

##### Format string
<table border="0">
  <thead>
    <tr>
      <th align="center">placeholder</th>
      <th align="center">substitution</th>
      <th align="center">additional info</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td align="right">%msg%</td>
      <td align="center">\$m</td>
      <td></td>
    <tr>
      <td align="right">%lvl%</td>
      <td align="center">\$l</td>
      <td>LOG, INFO, ERR, WARN or DEBUG</td>
    </tr>
    <tr>
      <td align="right">%cmd%</td>
      <td align="center">\$0</td>
      <td></td>
    </tr>
    <tr>
      <td align="right">%src%</td>
      <td align="center">\${FUNCNAME[\$d]}</td>
      <td>depending on log(...) call args, $d is either 1 or 2</td>
    </tr>
    <tr>
      <td align="right">%pcs%</td>
      <td align="center">\${FUNCNAME[*]}</td>
      <td></td>
    </tr>
    <tr>
      <td align="right">%pwd%</td>
      <td align="center">\${PWD}</td>
      <td></td>
    </tr>
    <tr>
      <td align="right">%usr%</td>
      <td align="center">\${USER}</td>
      <td></td>
    </tr>
    <tr>
      <td align="right">%:t.*%</td>
      <td align="center">$(parse_time \1)</td>
      <td>uses printf strftime formatting with number of fields and
          separators as specified after t to output relative time since
          logging library import statement</td>
    </tr>
    <tr>
      <td align="right">%:T.*%</td>
      <td align="center">$(parse_time \1)</td>
      <td>uses printf strftime formatting with number of fields and
          separators as specified after t to output absolute time</td>
    </tr>
  </tbody>
</table>
